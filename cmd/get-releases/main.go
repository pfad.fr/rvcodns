// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later WITH GPL-3.0-linking-source-exception

/*
get-releases retrieves and prints out the currently supported releases using [Release Version Check over DNS (RVCoDNS)].

Example usage:

	get-releases example.rvcodns.pfad.fr

Output:

	Version: 1.2.3
			answer=42
			other_key=spaced value

[Release Version Check over DNS (RVCoDNS)]: https://codeberg.org/forgejo-contrib/rvcodns
*/
package main

import (
	"fmt"
	"log"
	"os"

	"code.pfad.fr/rvcodns"
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		log.Fatalf("expected exactly one argument: domain to check")
	}
	releases, err := rvcodns.LookupReleases(args[0])
	if err != nil {
		log.Fatalf("could not lookup releases: %v", err)
	}
	for _, r := range releases {
		fmt.Println("Version:", r.Version)
		for _, t := range r.Tags {
			if t.Key == "v" {
				continue
			}
			fmt.Println("\t" + t.Key + "=" + t.Value.String())
		}
	}
}

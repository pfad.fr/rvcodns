// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later WITH GPL-3.0-linking-source-exception

/*
dkim-encode-pairs endodes key-value pairs for usage in [Release Version Check over DNS (RVCoDNS)].

Example usage:

	dkim-encode-pairs v 1.2.3 other_key other_value third_key "third value"

Output:

	v=1.2.3;other_key=other_value;third_key=third=20value

[Release Version Check over DNS (RVCoDNS)]: https://codeberg.org/forgejo-contrib/rvcodns
*/
package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"code.pfad.fr/rvcodns"
)

func main() {
	args := os.Args[1:]
	if len(args)%2 != 0 {
		log.Fatalf("expected even number of arguments (alternating key and value) got %d", len(args))
	}
	tags := make([]string, 0, len(args)/2)
	var key string
	for i := 0; i < len(args); i++ {
		a := args[i]
		if i%2 == 0 {
			key = a
		} else {
			tags = append(tags, key+"="+rvcodns.DKIMQuotedPrintableEncode(a))
		}
	}
	fmt.Print(strings.Join(tags, ";"))
}

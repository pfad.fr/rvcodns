// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later WITH GPL-3.0-linking-source-exception

package rvcodns_test

import (
	"context"
	"reflect"
	"testing"

	"code.pfad.fr/rvcodns"
)

func TestLookup(t *testing.T) {
	resolver := rvcodns.Resolver{
		LookupTXT: func(ctx context.Context, name string) ([]string, error) {
			if name != "_release.example.org" {
				t.Fatalf("unexpected name lookup: %s", name)
			}
			return []string{"v=1.2.3"}, nil
		},
	}
	releases, err := resolver.LookupReleases(context.Background(), "example.org")
	if err != nil {
		t.Fatal(err)
	}
	expected := []rvcodns.Release{
		{
			Version: "1.2.3",
			Tags:    []rvcodns.Tag{{"v", "1.2.3"}},
		},
	}
	if !reflect.DeepEqual(expected, releases) {
		t.Fatalf("Unexpected output, expected %q, got %q", expected, releases)
	}
}

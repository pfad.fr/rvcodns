// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
// SPDX-License-Identifier: CC0-1.0

package rvcodns_test

import (
	"fmt"

	"code.pfad.fr/rvcodns"
)

var domain = "example.rvcodns.pfad.fr"

func Example() {
	releases, err := rvcodns.LookupReleases(domain)
	if err != nil {
		panic(err) // network or parsing error (to handle)
	}
	for _, r := range releases {
		fmt.Println(r.Version) // compare with own version (to inform the user about available updates)
	}
	// Output: 1.2.3
}

// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later WITH GPL-3.0-linking-source-exception

package rvcodns

import (
	"testing"
	"testing/quick"
)

func TestValueDecoding(t *testing.T) {
	cases := []struct {
		Name, Input, Ouput string
	}{
		{
			"Simple",
			"1.2.3",
			"1.2.3",
		},
		{
			"whitespace removal",
			" 1.2.  \r\n3 ",
			"1.2.3",
		},
		{
			"hex-octet replacement",
			"1=2B1=20==3D=202",
			"1+1 == 2",
		},
		{
			"dkim-safe-char",
			"=21=! =3A=: =3C=< =3E=> =7E=~",
			"!=!:=:<=<>=>~=~",
		},
	}
	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			out := Value(c.Input).String()
			if c.Ouput != out {
				t.Fatalf("Unexpected output, expected %q, got %q", c.Ouput, out)
			}
		})
	}
}

func TestValueEncoding(t *testing.T) {
	cases := []struct {
		Name, Input, Ouput string
	}{
		{
			"Simple",
			"1.2.3",
			"1.2.3",
		},
		{
			"first category borders",
			" !09:;",
			"=20!09:=3B",
		},
		{
			"< borders",
			";<=",
			"=3B<=3D",
		},
		{
			"last category borders",
			"=>?@AZ[_`az{|}~",
			"=3D>?@AZ[_`az{|}~",
		},
		{
			"invalid UTF-8",
			"\U0008fc95",
			"=F2=8F=B2=95",
		},
	}
	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			out := DKIMQuotedPrintableEncode(c.Input)
			if c.Ouput != out {
				t.Fatalf("Unexpected output, expected %q, got %q", c.Ouput, out)
			}
		})
	}

	encodeDecode := func(s string) bool {
		encoded := DKIMQuotedPrintableEncode(s)
		decoded := Value(encoded).String()
		return s == decoded
	}

	if err := quick.Check(encodeDecode, nil); err != nil {
		t.Error(err)
	}
}

// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later WITH GPL-3.0-linking-source-exception

package rvcodns

import (
	"reflect"
	"testing"
)

func TestParse(t *testing.T) {
	cases := []struct {
		Name, Input string
		Ouput       Release
	}{
		{
			"Simple",
			"v=1.2.3",
			Release{
				Version: "1.2.3",
				Tags:    []Tag{{"v", "1.2.3"}},
			},
		},
		{
			"Escaped",
			"v=0.2.0=2Bincompatible",
			Release{
				Version: "0.2.0+incompatible",
				Tags:    []Tag{{"v", "0.2.0=2Bincompatible"}},
			},
		},
		{
			"Multiple tags",
			"v=0.2.1;kind=LTS",
			Release{
				Version: "0.2.1",
				Tags:    []Tag{{"v", "0.2.1"}, {"kind", "LTS"}},
			},
		},
		{
			"Version not first",
			"kind=LTS;v=0.2.1",
			Release{
				Version: "0.2.1",
				Tags:    []Tag{{"kind", "LTS"}, {"v", "0.2.1"}},
			},
		},
	}
	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			out, err := Parse(c.Input)
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(c.Ouput, out) {
				t.Fatalf("Unexpected output, expected %q, got %q", c.Ouput, out)
			}
		})
	}
}

func TestParseMultiple(t *testing.T) {
	_, err := Parse("v=1.0;v=2.0")
	if err == nil || err.Error() != "unexpected duplicated tag: v" {
		t.Fatalf("unexpected error: %s", err)
	}
}

func TestParseEmptyVersion(t *testing.T) {
	_, err := Parse("v=")
	if err == nil || err.Error() != "empty version tag: v" {
		t.Fatalf("unexpected error: %s", err)
	}
}

func TestParseEmptyKey(t *testing.T) {
	_, err := Parse("v=1.2.3;")
	if err == nil || err.Error() != "empty key" {
		t.Fatalf("unexpected error: %s", err)
	}
}

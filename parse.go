// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later WITH GPL-3.0-linking-source-exception

package rvcodns

import (
	"errors"
	"strings"
)

// Release represents a release record
type Release struct {
	Version string // decoded version string
	Tags    []Tag  // raw tags in the order they appeared
}

// Tag represents a key-value tag pair of the DNS record
type Tag struct {
	Key   string
	Value Value
}

var errDuplicateVTag = errors.New("unexpected duplicated tag: v")
var errEmptyVTag = errors.New("empty version tag: v")
var errEmptyKey = errors.New("empty key")

// Parse parses a DNS records into a [Release]
func Parse(record string) (Release, error) {
	r := Release{}
	foundVTag := false
	for _, tag := range strings.Split(record, ";") {
		key, rawValue, _ := strings.Cut(tag, "=")
		key = strings.TrimSpace(key)
		if key == "" {
			return r, errEmptyKey
		}
		value := Value(strings.TrimSpace(rawValue))

		if key == "v" {
			v := value.String()
			if v == "" {
				return r, errEmptyVTag
			}
			if foundVTag {
				return r, errDuplicateVTag
			}
			r.Version = v
			foundVTag = true
		}
		r.Tags = append(r.Tags, Tag{
			Key:   key,
			Value: value,
		})
	}
	return r, nil
}

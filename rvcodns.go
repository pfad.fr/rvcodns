// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later WITH GPL-3.0-linking-source-exception

/*
package rvcodns provides a Go client for [Release Version Check over DNS (RVCoDNS)].

[Release Version Check over DNS (RVCoDNS)]: https://codeberg.org/forgejo-contrib/rvcodns
*/
package rvcodns

import (
	"context"
	"errors"
	"fmt"
	"net"
)

// Resolver allows to specify the LookupTXT function to use
// (usually [net.DefaultResolver.LookupTXT])
type Resolver struct {
	LookupTXT func(ctx context.Context, name string) ([]string, error)
}

// DefaultResolver is the resolver used by the package-level LookupRelease
// function.
var DefaultResolver = Resolver{net.DefaultResolver.LookupTXT}

// LookupReleases looks up and parses supported releases by querying the DNS TXT "_release.{domain}" records.
func (r Resolver) LookupReleases(ctx context.Context, domain string) ([]Release, error) {
	rawRecords, err := r.LookupTXT(ctx, "_release."+domain)
	if err != nil {
		return nil, err
	}
	records := make([]Release, 0, len(rawRecords))
	var errs []error
	for _, raw := range rawRecords {
		rec, err := Parse(raw)
		if err != nil {
			errs = append(errs, fmt.Errorf("could not parse _release record %q: %v", raw, err))
		}
		records = append(records, rec)
	}
	return records, errors.Join(errs...)
}

// LookupReleases looks up and parses supported releases by DNS-querying _release.{domain}
//
// LookupReleases uses context.Background internally; to specify the context, use
// [Resolver.LookupReleases].
func LookupReleases(domain string) ([]Release, error) {
	return DefaultResolver.LookupReleases(context.Background(), domain)
}

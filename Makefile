# SPDX-FileCopyrightText: 2023 Olivier Charvin
# SPDX-License-Identifier: CC0-1.0

test:
	go test ./...
	reuse lint

doc:
	inotifywait -m -e close_write --include "\.go$$" -r . | \
		devf http://localhost:8181 -- vanitydoc -addr localhost:8181
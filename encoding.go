// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later WITH GPL-3.0-linking-source-exception

package rvcodns

import (
	"fmt"
	"regexp"
	"strings"
	"unicode"
)

// Value represents a raw DNS value. Use [Value.String] to get the decoded value.
type Value string

func removeSpaces(str string) string {
	var b strings.Builder
	b.Grow(len(str))
	for _, r := range str {
		if unicode.IsSpace(r) {
			continue
		}
		b.WriteRune(r)
	}
	return b.String()
}

var hexOctet = regexp.MustCompile("=([0-9A-F]{2})")

func hexToByte(b byte) byte {
	switch b {
	case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
		return b - '0'
	case 'A', 'B', 'C', 'D', 'E', 'F':
		return b - 'A' + 10
	}
	panic(fmt.Sprintf("non-hexadecimal byte: %s", string(b)))
}

// String parses the value as a [DKIM-Quoted-Printable (RFC 6376)] string.
// It removes whitespaces and decodes hex-octets "=XX".
//
// See [DKIMQuotedPrintableEncode] for encoding.
//
// [DKIM-Quoted-Printable (RFC 6376)]: https://www.rfc-editor.org/rfc/rfc6376#section-2.11
func (v Value) String() string {
	printable := removeSpaces(string(v))
	printable = hexOctet.ReplaceAllStringFunc(printable, func(octet string) string {
		return string([]byte{0x10*hexToByte(octet[1]) + hexToByte(octet[2])}) // []byte{} cast needed in case of invalid UTF-8 rune
	})
	return printable
}

const hextable = "0123456789ABCDEF"

// DKIMQuotedPrintableEncode encodes a strings as a [DKIM-Quoted-Printable (RFC 6376)] string.
//
// See [Value.String] for decoding.
//
// [DKIM-Quoted-Printable (RFC 6376)]: https://www.rfc-editor.org/rfc/rfc6376#section-2.11
func DKIMQuotedPrintableEncode(str string) string {
	var res strings.Builder
	res.Grow(len(str))
	for _, b := range []byte(str) {
		if (b >= '!' && b <= ':') ||
			b == '<' ||
			(b >= '>' && b <= '~') {
			res.WriteByte(b)
			continue
		}

		res.WriteByte('=')
		res.WriteByte(hextable[b>>4])
		res.WriteByte(hextable[b&0x0f])
	}
	return res.String()
}
